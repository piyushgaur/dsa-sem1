public class App {
    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");
        CircularQ o = new CircularQ(5);
        o.enQ(1);
        // System.out.print(o.deQ());
        o.enQ(2);
        o.enQ(3);
        // System.out.print(o.deQ());
        o.enQ(4);
        o.enQ(5);
        System.out.print(o.deQ());
        System.out.print(o.deQ());
        o.enQ(100);
        o.enQ(200);

        System.out.println("currently q:");
        o.display();
        System.out.println();
        System.out.print(o.deQ());
        System.out.print(o.deQ());
        System.out.print(o.deQ());
        System.out.print(o.deQ());
    }
}
