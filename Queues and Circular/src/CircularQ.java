public class CircularQ {
    // front and rear start from 0
    int f, r, size;
    int[] q;

    CircularQ(int s) {
        size = s + 1;
        f = 0;
        r = 0;
        q = new int[size];
    }

    private boolean isEmpty() {
        return f == r;
    }

    private boolean isFull() {
        return (r + 1) % size == (f % size);
    }

    public void enQ(int data) {
        if (isFull()) {
            System.out.println("q is full");
        } else {
            r = (r + 1) % size;
            q[r] = data;
        }
    }

    public int deQ() {
        int x = -1;
        if (isEmpty()) {
            System.out.println("q is empty can't delete");
        } else {
            f = (f + 1) % size;
            x = q[f];
        }
        return x;
    }

    public void display() {
        /**
         * 1 2 3 4 x
         * r f
         * 
         */
        int i = (f + 1) % size;
        while (i != r % size) {
            System.out.print(q[i] + ", ");
            i = (i + 1) % size;
        }
        System.out.println(q[i]);
    }
}
