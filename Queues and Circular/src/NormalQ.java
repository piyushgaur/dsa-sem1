public class NormalQ {
    int[] q;
    int size;
    int front, rear;

    NormalQ(int s) {
        size = s;
        q = new int[size];
        front = -1;
        rear = -1;
    }

    private boolean isEmpty() {
        return front == rear;
    }

    private boolean isFull() {
        return rear == size - 1;
    }

    public void enQ(int data) {
        if (isFull()) {
            System.out.println("q is full cannot enter");
        } else {
            rear++;
            q[rear] = data;
        }
    }

    public int deQ() {
        int x = -1;
        if (isEmpty()) {
            System.out.println("cannot delete q is empty");
        } else {
            front++;
            x = q[front];
        }
        return x;
    }

    public void displayQ() {
        for (int i = front + 1; i <= rear; i++) {
            System.out.print(q[i] + ", ");
        }
    }
}
