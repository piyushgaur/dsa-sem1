public class Flames {
    Node head, temp, p, q;
    String s;

    Flames() {
        head = null;
        p = null;
        temp = null;
        s = "flames";
    }

    public void genFlames() {
        for (int i = 0; i < s.length(); i++) {
            insertAtLast(s.charAt(i));
        }
    }

    private void insertAtLast(char x) {
        Node n = new Node(x);
        if (isEmpty()) {
            head = n;
            temp = head;
        } else {
            temp.next = n;
            temp = temp.next;
            temp.next = head;
        }
    }

    private void deleteNode(int x) {
        // delete from circular ll
        int pos = x % 6;

        if (pos == 1) {
            p = head;
            while (p.next != head) {
                p = p.next;
            }
            if (p == head) {
                head = null;
            } else {
                p.next = head.next;
                head = head.next;
            }
        } else {
            p = head;
            for (int i = 0; i < pos - 2; i++) {
                p = p.next;
            }
            q = p.next;
            p.next = q.next;
        }
    }

    private boolean isEmpty() {
        return head == null;
    }

    public void displayLL() {
        p = head;
        do {
            System.out.print(p.data + "--> ");
            p = p.next;
        } while (p != head);
        System.out.println("end");
    }

    public void calScore(String n1, String n2) {
        char[] x = n1.toLowerCase().toCharArray();
        char[] y = n2.toLowerCase().toCharArray();
        int common = 0, uncommon = 0;

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < y.length; j++) {
                if (x[i] == y[j]) {
                    x[i] = '*';
                    y[j] = '*';
                    break;
                }
            }
        }

        for (char c : x) {
            if (c == '*') {
                common++;
            }
        }

        for (char c : y) {
            if (c == '*') {
                common++;
            }
        }

        if (common == x.length + y.length) {
            System.out.println("cannot generate flames");
        } else {
            uncommon = (x.length + y.length) - common;
            // delete every uncommon letter
            generateResult(uncommon);
        }
    }

    private void generateResult(int unc) {
        for (int i = 0; i < unc - 1; i++) {
            deleteNode(unc);
        }
        // displayLL();
        switch (head.next.data) {
            case 'f': {
                System.out.println("friendship");
                break;
            }
            case 'l': {
                System.out.println("love");
                break;
            }
            case 'a': {
                System.out.println("affection");
                break;
            }
            case 'm': {
                System.out.println("marriage");
                break;
            }
            case 'e': {
                System.out.println("enemies");
                break;
            }
            case 's': {
                System.out.println("sister");
                break;
            }

            default:
                break;
        }
    }
}
