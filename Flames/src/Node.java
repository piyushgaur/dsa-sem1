public class Node {
    char data;
    Node next;

    Node() {
        next = null;
    }

    Node(char data) {
        this.data = data;
        next = null;
    }
}
