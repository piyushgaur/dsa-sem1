import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Graph {
    ArrayList<ArrayList<Integer>> g;
    int v;

    Graph(int v) {
        g = new ArrayList<>(v);
        this.v = v;
        // initialization
        for (int i = 0; i < v; i++) {
            g.add(new ArrayList<>());
        }
    }

    public void addEdge(int s, int d) {
        // undirected graph
        g.get(s).add(d);
        g.get(d).add(s);
    }

    public void displayGraph() {
        for (int i = 0; i < g.size(); i++) {
            System.out.print(i + "--> ");
            for (int j = 0; j < g.get(i).size(); j++) {
                System.out.print(g.get(i).get(j) + ", ");
            }
            System.out.println();
        }
    }

    public void bfsUtil() {
        boolean[] vis = new boolean[v];
        for (int i = 0; i < v; i++) {
            if (!vis[i]) {
                bfs(i, vis);
            }
        }
    }

    private void bfs(int s, boolean[] vis) {
        Queue<Integer> q = new LinkedList<>();
        q.offer(s);
        vis[s] = true;

        while (!q.isEmpty()) {
            int u = q.poll();
            System.out.print(u + ", ");

            for (Integer xInteger : g.get(s)) {
                if (!vis[xInteger]) {
                    vis[xInteger] = true;
                    q.offer(xInteger);
                }
            }
        }
    }

    public void dfsUtil() {
        boolean[] vis = new boolean[v];
        for (int i = 0; i < v; i++) {
            if (!vis[i]) {
                dfs(i, vis);
            }
        }
    }

    private void dfs(int s, boolean[] vis) {
        vis[s] = true;
        System.out.print(s + ", ");

        for (Integer x : g.get(s)) {
            if (!vis[x]) {
                dfs(x, vis);
            }
        }
    }
}
