import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        int[] arr = { 11, 22, 33, 44, 55, 66, 77, 88, 99, 111, 222, 333, 444 };
        Scanner in = new Scanner(System.in);
        Solution o = new Solution();

        System.out.print("enter a key to search: ");
        int key = in.nextInt();

        o.search(arr, key);
    }
}

/**
 * Solution
 */
class Solution {
    public void search(int[] arr, int key) {
        int res = searchUtil(arr, key);
        if (res == -1) {
            System.out.println("not found");
        } else {
            System.out.println(key + " found at: " + res);
        }
    }

    private int searchUtil(int[] arr, int key) {
        int lo = 0, hi = arr.length - 1, mid = 0, x = -1;

        while (lo <= hi) {
            mid = lo + (hi - lo) / 2;
            if (key < arr[mid]) {
                hi = mid - 1;
            } else if (key > arr[mid]) {
                lo = mid + 1;
            } else {
                x = mid;
                break;
            }
        }
        return x;
    }
}
