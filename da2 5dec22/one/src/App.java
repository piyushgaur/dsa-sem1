import java.util.HashMap;

public class App {
    public static void main(String[] args) throws Exception {
        int[] inp = { 10, 4, -3, -1, 0, 4, 3, -15, -8, 4, -1, 9, 3, 1, 7 };
        Solution o = new Solution();
        o.sol(inp);
    }
}

/**
 * Solution
 */
class Solution {
    int[] neg, pos;
    int negcomp = 0, pcomp = 0;

    public void sol(int[] inp) {
        divideArr(inp);
        // array is divided in pos and neg arrays
        // now we'll sort
        pcomp = sort(pos);
        System.out.println("1st array");
        display(pos);
        System.out.println("no. of comparisions: " + pcomp);
        System.out.println("frequencies: ");
        frequency(pos);

        negcomp = sort(neg);
        System.out.println("2nd array");
        display(neg);
        System.out.println("no. of comparisions: " + negcomp);
        System.out.println("frequencies: ");
        frequency(neg);

    }

    public void divideArr(int[] inp) {
        int n = inp.length, k = 0, j = 0, cnt = 0;

        for (int i = 0; i < n; i++) {
            if (inp[i] < 0) {
                cnt++;
            }
        }
        neg = new int[cnt];
        pos = new int[n - cnt + 1];

        for (int i = 0; i < n; i++) {
            if (inp[i] < 0) {
                neg[j++] = inp[i];
            } else {
                pos[k++] = inp[i];
            }
        }
    }

    public int sort(int[] arr) {
        // we'll use insertion sort
        int n = arr.length, x, j, cnt = 0;

        for (int i = 1; i < n - 1; i++) {
            j = i - 1;
            x = arr[i];

            while (j > -1 && x < arr[j]) {
                arr[j + 1] = arr[j];
                j--;
                cnt++;
            }
            arr[j + 1] = x;
        }
        return cnt;
    }

    public void frequency(int[] arr) {
        HashMap<Integer, Integer> mp = new HashMap<>();

        for (int i : arr) {
            mp.put(i, mp.getOrDefault(i, 0) + 1);
        }

        for (Integer x : mp.keySet()) {
            System.out.println(x + ": " + mp.get(x));
        }
    }

    public void display(int[] arr) {
        for (int i : arr) {
            System.out.print(i + ", ");
        }
        System.out.println();
    }
}