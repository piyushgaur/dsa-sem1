import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        int mst, totMst;
        Scanner in = new Scanner(System.in);
        int[][] edges = {
                { -1, 7, 8, -1, -1 },
                { 7, -1, 3, 6, 5 },
                { 8, 3, -1, 4, 3 },
                { -1, 6, 4, -1, 2 },
                { -1, 5, 3, 2, -1 }
        };
        Prims o = new Prims();
        mst = o.minSpanningTree(edges);
        System.out.print("enter the total cost: ");
        totMst = in.nextInt();
        System.out.println("calculated cost of mst is: " + mst);
        System.out.println("the missing edge value is: " + (totMst - mst));
    }
}
