import java.util.ArrayList;
import java.util.PriorityQueue;

public class Prims {

    class Pair {
        int node, distance;

        Pair(int n, int d) {
            node = n;
            distance = d;
        }
    }

    public int minSpanningTree(int[][] edges) {
        int res = 0, N = edges.length, u, v, d;
        boolean[] vis = new boolean[N];
        ArrayList<ArrayList<Pair>> graph = new ArrayList<>();
        PriorityQueue<Pair> heap = new PriorityQueue<>((a, b) -> (a.distance - b.distance));
        /**
         * min heap, sorts in ascending order of distance length.
         */

        for (int i = 0; i < N; i++) {
            // initialization
            graph.add(new ArrayList<>());
        }

        // adjacency matrix to adjacency list.
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (edges[i][j] != -1) {
                    u = i;
                    v = j;
                    d = edges[i][j];

                    graph.get(u).add(new Pair(v, d));
                }
            }
        }

        heap.offer(new Pair(0, 0));

        while (!heap.isEmpty()) {
            Pair p = heap.poll();
            v = p.node;
            d = p.distance;

            if (!vis[v]) {
                vis[v] = true;
                res += d;

                for (Pair x : graph.get(v)) {
                    if (!vis[x.node]) {
                        heap.offer(new Pair(x.node, x.distance));
                    }
                }
            }
        }

        return res;
    }
}
